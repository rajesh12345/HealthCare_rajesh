package com.health.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class Calculator {
    private final static Logger logger = LoggerFactory.getLogger(Calculator.class);
    final int LESS_THAN_25 = 10;
    final int LESS_THAN_30 = 20;
    final int LESS_THAN_35 = 30;
    final int LESS_THAN_40 = 40;
    final int GREATER_THAN_40 = 60;
    final int BASE_VALUE = 5000;

    final int MALE = 2;

    protected BigDecimal computePremium(InputPojo inputPojo) {


        BigDecimal totPercent = BigDecimal.ZERO;

        if(inputPojo.getHypertension() || inputPojo.getBloodPressure() || inputPojo.getBloodsugar() || inputPojo.getOverweight()){
            totPercent = totPercent.add(BigDecimal.ONE);
        }

        if(inputPojo.getDailyExercise()){
            totPercent = totPercent.subtract(new BigDecimal(3));
        }

        if(inputPojo.getSmoking() || inputPojo.getAlcohol() || inputPojo.getDrugs()){
            totPercent = totPercent.add(new BigDecimal(3));
        }

        if(inputPojo.getGender() == "MALE"){
            totPercent = totPercent.add(new BigDecimal(MALE));
        }

        if(inputPojo.getAge().intValue() > 40)
            totPercent = totPercent.add(new BigDecimal(GREATER_THAN_40));
        else if(inputPojo.getAge().intValue() <= 40)
            totPercent = totPercent.add(new BigDecimal(LESS_THAN_40));
        else if(inputPojo.getAge().intValue() <= 35)
            totPercent = totPercent.add(new BigDecimal(LESS_THAN_35));
        else if(inputPojo.getAge().intValue() <= 30)
            totPercent = totPercent.add(new BigDecimal(LESS_THAN_30));
        else if(inputPojo.getAge().intValue() <= 25)
            totPercent = totPercent.add(new BigDecimal(LESS_THAN_25));


        BigDecimal premium = new BigDecimal(BASE_VALUE).multiply(totPercent.divide(new BigDecimal(100)));

        logger.debug("Name : {} Age : {} Gender : {} Hypertension : {} BloodPressure : {} BloodSugar : {} Overweight : {}" +
                "Smoking : {} alcohol : {} DailyExercise : {} Drugs : {} Premium : {}",inputPojo.getName(),inputPojo.getAge(),inputPojo.getGender(),inputPojo.getHypertension(),inputPojo.getBloodPressure(),
                inputPojo.getBloodsugar(),inputPojo.getOverweight(),inputPojo.getSmoking(),inputPojo.getAlcohol(),inputPojo.getDailyExercise(),inputPojo.getDrugs(),premium);
        return premium;
    }
}
